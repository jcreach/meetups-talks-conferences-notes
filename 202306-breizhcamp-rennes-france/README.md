# BreizhCamp 2023

## Table of content

[[_TOC_]]

## Mercredi

### 11h00 - 11h55 (Amphi D) Playwright : l'outil qui va révolutionner les tests end-to-end

**Abstract**

Speaker : Jean-François Greffier

```
Certaines équipes hésitent ou ne mettent pas en place de tests end-to-end. "Trop dur à mettre en place" "Tests difficiles à maintenir" "Perte d'argent et de temps"

Dans cette conférence, je vous propose de partir à la découverte de Playwright. Un nouvel outil Microsoft, rapide, fiable et complet, qui va probablement changer vos aprioris et appréhensions sur les tests sur navigateurs.

À travers live-coding, démos et exemples, nous verrons comment créer des tests robustes. Nous évoquerons aussi les performances, les limitations à prendre en compte, l'outillage ou encore comment débugger, même après coup.

Venez découvrir le futur des tests end-to-end !
```

### 13h30 - 15h30 (Amphi A) Docker Compose, zero to hero

**Abstract**

Speaker : Nicolas De Loof, Guillaume Lours

```
Vous avez déjà entendu parlé de Docker Compose ? Peut être l'utilisez vous déjà un peu ? Nous vous proposons de découvrir toutes les facettes de cet outil en construisant une application ensemble, de manière itérative et en 100% live coding.

Depuis un exemple simple, nous aborderons toutes les astuces pour être un développeur efficace: services multiples, gestion des volumes et du réseau, build avancé avec des ressources protégées par ssh, multi-plateformes...
```

### 16h00 - 16h55 (Amphi D) Les machines à états, une documentation exécutable ? (et debuggable ? et testable ? 🤫)

**Abstract**

Speaker : Antoine Cailly

```
Les machine à états, pour visualiser la logique de fonctionnement d'une application sous forme d'un diagramme, c'est vieux comme le monde 👴 Alors pourquoi ressortir ça maintenant ?

Parce qu'un nouvel éditeur basé sur xstate est arrivé, stately, et il semble offrir un moyen aux non devs (testeur, PO, whatever) de visualiser et éditer ces fameuses machines

Ces machines à état étant ensuite exécutées dans l'appli, on aurait un code et une doc toujours synchro et compréhensible, le rêve 🤩 Attendez, il y a mieux ! Il paraît qu'on peut générer des tests e2e depuis ces machines à états 🤯

Alors essayons, j'ai pris une petite appli perso et j'ai tenté de voir ce que ca donnait 🧪
```

### 17h05 - 18h00 (Amphi D) Comment aborder le design dans une démarche d’éco-conception ?

**Abstract**

Speaker : Damien Legendre

```
Les chiffres alarmants de l’impact du numérique sur le climat sont clairs. Il est désormais temps de comprendre comment passer du constat à la mise en pratique. Cette conférence vise un but simple : aider les designers et ceux qui créent le web avec eux à concevoir des interfaces à la fois utiles et mesurées en regard de l'impact climatique du numérique.

Comment nous, designers et concepteurs pouvons jouer un rôle clef dès les fondations du projets, pour éviter de produire des services web obèses et disproportionnés ?

À l’issue de ce talk, vous repartirez avec des leviers pour convaincre, mais surtout des méthodes et outils simples, applicables sur le champ pour vos futurs projets éco-conçus, issues de nos recherches et éprouvées sur nos projets récents.
```

## Jeudi

### 9h - 10h (Amphi C) Keynote

**Abstract**

Speaker :

```

```

### 10h30 - 11h25 (Amphi B) 3P pour sortir du Legacy

**Abstract**

Speaker : Johan Martinsson

```
Quand on écrit des tests ou fait du refactoring en fin de story, qui récolte les bénéfices? Et quand?

C'est la prochaine personne qui touchera le code ... d'ici quelque temps ... avec un peu de chance. Pourvu que les tests resteront pertinents. Et que le refactoring facilitera bien la nouvelle fonctionnalité, que l'on ne connait pas encore ... 🤔

Si nos efforts sont premièrement pour les autres, dans un futur incertain et de valeur incertaine. Est-ce étonnant si on ne fait pas beaucoup et qu'on laisse pourrir nos logiciels?

Une autre approche serait la méthode des 3P 🧐. C'est à dire pour chaque story

    - Protéger
    - Préparer
    - Produire

Protéger: Mise en place des tests manquants

Préparer: Refactoring en profondeur pour rendre plus facile la nouvelle fonctionnalité

Produire: Coder la nouvelle fonctionnalité en TDD

Être les premiers bénéficiaires des tests et du refactoring que nous faisons, c'est plus de bonheur au travail. Avec un ROI bien meilleur sur les tests et le refactoring, le projet risque de mieux se porter dans le temps. Et pour les adeptes du TDD cela vous permettra de faire du TDD quelque soit la qualité du code.

Regardons quelques exemples en code pour clarifier ce concept
```

### 11h35 - 12h30 (Amphi D) S'attaquer à un legacy grâce au DDD

**Abstract**

Speaker : Tanguy Bernard

```
Dans une vie de développeur, il arrive bien souvent que le métier sur lequel nous intervenons se complexifie. La solution mise en place doit alors évoluer. S'attaquer à ce genre de complexité sur un legacy bien en place peut être fastidieux.

Je propose dans ce talk de vous partager 4 stratégies mises en avant par Eric Evans (Auteur de Domain-Driven Design: Tackling Complexity in Software) pour aborder et faire évoluer votre legacy grâce au DDD.

Je vous partagerai également mon expérience suite à l'implémentation de ces stratégies.
```

### 12h45 - (Amphi D) Doc-tracing : fouiller une base de code fossile grâce au traçage d'exécution

**Abstract**

Speaker : Luc Sorel-Giffo

```
Vous est-il déjà arrivé d'arriver sur un projet préhistorique et de vous sentir démuni·e en fouillant la base de code pour comprendre son organisation et la façon dont les fonctionnalités sont implémentées ? Malgré la doc générée, vous vous perdez sur la route du clic et dans les onglets de votre IDE 😬.

Des outils de doc-tracing permettent de tracer l'exécution du code et de la documenter le diplodoc-as-codus : diagramme de séquence, diagramme des composants mobilisés par exemple.

À travers différentes démos, je vous présenterai comment l'outil pydoctrace utilise les hooks de l'interpréteur Python pour générer ces diagrammes en apposant un simple décorateur (similaire à une annotation des langages JVM) sur une fonction ou un test fonctionnel.

Plan :

    - utilité et exemples de doc-as-code
    - mécanismes de traçage d’exécution de code Python
    - partage de résultats de doc-tracing sur des tests de cas droits et d’erreurs
```

### 13h30 - 14h25 (Amphi B) Infrastructure as Code ou Infrastructure as Software ?

**Abstract**

Speaker : Alexandre Nédélec

```
Alors que la plupart des applications que nous développons sont désormais cloud-native, automatiser la création de leurs infrastructures via de l’Infrastructure as Code est devenu un incontournable.

Mais est-ce qu'au-delà d'écrire du "code" pour décrire notre infra, on ne devrait pas développer l'infra comme n'importe quel logiciel ?

C'est l'approche privilégiée par certaines solutions d'IaC dont Pulumi, une plateforme open-source d'infrastructure as software que l’on abordera plus en détails dans cette présentation. Ce sera également l'occasion de discuter un peu des avantages et inconvénients des différentes solutions d'IaC.

Si comme moi vous :

    - voulez que votre infrastructure fasse partie intégrante de votre software et soit traitée comme le reste de votre code (build, tests unitaires, packages de composants réutilisables, ..)
    - préférez le Kotlin/Java/C#/Typescript/Python/Go au JSON, au YAML ou à apprendre un nouveau DSL
    aimez garder le confort de votre IDE et de son auto-complétion pour écrire votre infra
    - souhaitez une solution d'Infrastructure as Code multi-providers qui vous permette d'automatiser un peu tout
    voulez provisionner interactivement votre infrastructure en langage naturel grace à l'IA

Alors ce talk est fait pour vous !
```

### 14h35 - 15h30 (Amphi B) La Merge Queue, la pièce manquante du CI/CD

**Abstract**

Speaker : Charly Laurent

```
Dans un monde en constante évolution, l’état de l’art en développement logiciel est crucial pour rester compétitif. Cette conférence vous dévoilera les dernières avancées en matière de développement.

Notre objectif en tant qu’équipe de développement est de concilier qualité, sécurité et rapidité. Livrer rapidement et fréquemment nos produits est essentiel pour conquérir le marché. Pour répondre à ces enjeux, nous automatisons les processus tels que les tests, l’intégration et le déploiement. Mais que reste-t-il à optimiser ? Quels sont les goulots d’étranglement qui persistent ?

Découvrez la Merge Queue, une solution innovante qui repousse les limites de l’automatisation en simplifiant et sécurisant la fusion du code. Nous présenterons des cas d’usage concrets démontrant les gains de temps significatifs pour les équipes de développement.

Que vous soyez un développeur expérimenté ou que vous cherchiez à en apprendre davantage sur les dernières tendances en matière de développement, cette conférence est l’opportunité parfaite pour enrichir vos connaissances et appréhender les défis complexes de notre industrie.

Points clés à retenir :

    - Restez informé sur les dernières avancées en matière de développement logiciel.
    - Comprenez l’importance de l’automatisation et de l’optimisation du pipeline CI/CD.
    - Découvrez la Merge Queue, une solution innovante pour sécuriser et faciliter la fusion de code.
    - Apprenez grâce à des cas d’usage concrets comment gagner du temps et améliorer la vélocité de vos équipes.

Ne manquez pas cette occasion unique de vous immerger dans les meilleures pratiques du développement logiciel afin d’optimiser votre pipeline CI/CD !
```

### 16h -16h55 (Amphi A) Pourquoi tester ce n’est pas douter

**Abstract**

Speaker : Édouard Siha, Gérôme Egron

```
TU, TI, tests E2E, tests API, tests fonctionnels, tests auto, etc. : beaucoup de noms qui n’ont pourtant pas nécessairement la même définition selon l’interlocuteur. Profitons de cette session pour clarifier tout cela.

Participez avec nous à un atelier 3 amigos afin de discuter d’une nouvelle fonctionnalité puis accompagnez-nous dans le développement de cette dernière avec une approche test first sur plusieurs niveaux.

Vous (re)découvrirez les avantages offerts par tout l’outillage autour des tests et quels bénéfices en tirer. À la fin de cette session, vous aurez fait des tests vos amis pour que tester ne soit plus jamais douter.
```

### 17h05 - 17h30 (Amphi A) Construire un registre de choix d'architecture avec les ADR

**Abstract**

Speaker : Sébastien Lecacheur

```
Pourquoi et comment tracer vos choix d'architecture ?

Venez découvrir une méthode pour tracer vos choix d'architecture de plus en plus populaire, qui nous fait gagner énormément de temps et qui augmente la connaissance des équipes. Cette méthode, utilisant les ADR (Architecture Decision Record), met en avant la simplicité : Keep It Simple, Stupid !

Au travers d'un retour d'expérience de plusieurs années, nous verrons donc comment documenter le plus simplement possible un choix d'architecture ou de conception tout en gardant une trace du contexte, des options et des compromis qui ont menés à cette décision.

Cette session sera également l'occasion de revenir sur la mise en pratique à l'échelle de l'entreprise.
```

### 17h35 - 18h (Amphi B) Pair-programming : et si on gagnait du temps ?

**Abstract**

Speaker : Pascal Le Merrer

```
L'inconvénient d'être un dinosaure, c'est que plus de 20 ans après avoir commencé à pratiquer le pair programming, j'entends encore parfois des remarques de développeurs sceptiques, telles que : "Seul, je serais plus efficace".

Nous verrons :

    - pourquoi c'est faux la plupart du temps ;
    - quels bénéfices une équipe peut tirer du pair programming, quand elle connait les pratiques pour le rendre efficace ;
    - quelle valeur vous pouvez apporter à vos collègues avec cette technique.

Allergène possible : ce talk contient des retours d'expérience.
```

### 18h05 - 18h50 (Amphi C) Keynote

**Abstract**

Speaker :

```

```

## Vendredi

### 9h - 10h (Amphi C) Keynote

**Abstract**

Speaker :

```

```

### 10h30 - 11h25 (Amphi C) Dans le Monorepo vous n'êtes jamais seul, le Park est ouvert... 🦖🦕🐢

**Abstract**

Speaker : Francois Nollen, Julien Buret

```
Quand un projet grossit et que les équipes se multiplient, naturellement on organise des dépôts Git pour chaque composant ou équipe. Chacun chez soi, les dinosaures seront bien gardés 🦕🐊🦤🐢🦎.

Popularisée par Google, Microsoft, Facebook ou Twitter, l'approche Monorepo propose au contraire un unique dépôt pour tous les applicatifs. Vous pensez qu'un Monorepo c'est juste pour aller vite sur un petit projet ? Que c'est folie à grande échelle ? Que ça signifie couplage et architecture Monolithique (voire Paléolithique) ?

Imaginez qu'on a rassemblé 300 dev Mobile, Web, Backend et Infra-as-code dans un même repository 🤯. Et ça marche ! Vous voulez savoir comment ?

Dans ce "ptit-REX" 🦖 vous découvrirez :

    - Les différences fondamentales entre Multirepos et Monorepo,
    - Les défis d'un Monorepo côté build (Gradle) et intégration continue (Gitlab, Terraform, AWS), comment les surmonter,
    - Les pratiques Open Source (Merge Requests, Code Reviews) qui améliorent la collaboration et répondent aux enjeux de qualité avec un rythme de développement soutenu à grande échelle,
    - Comment un Monorepo a permis d'accélérer nos développements, vers le Continuous Delivery.

Bienvenue dans le Park du Monorepo 🏞️ 🤠. Découvrez un monde différent, séduisant et parfois dangereux. Dans le Monorepo vous n'êtes jamais seul(e). Mais soyez rassuré(e), ça se termine bien 🍿.
```

### 11h35 - 12h30 (Amphi B) Git, sous le capot

**Abstract**

Speaker : David Blanchet

```
Vous a-t-on déjà sorti cette phrase :

  "Git is smart on the outside, and quite dumb on the inside."

?

Ça fait des semaines, des mois, voire des années que vous utilisez Git. C'est comme conduire une voiture, c'est devenu machinal. les commandes s'enchaînent, ça fait le boulot, pas besoin de savoir comment ça marche à l'intérieur pour s'en servir.

Et c'est bien dommage, car les fondements de Git sont beaucoup plus simples qu'il n'y paraît. Au point qu'on peut facilement les exposer et même les implémenter !

Dans ce talk, je vous propose de regarder sous le capot de cette belle machine et de voir comment elle fonctionne, d'abord parce que c'est cool, et ensuite parce que bien comprendre vos outils, c'est aussi mieux les exploiter.
```

### 12h45 (Amphi B) sécurité + open source = ❤️

**Abstract**

Speaker : Adrien Pessu

```
Pour beaucoup de développeurs , l'open source ce sont des frameworks ou des librairies qui apportent des fonctionnalités. On parle peu de l'open source dans la sécurité et quand on en parle c'est pour dire qu'il y a des vulnérabilités. Mais dans le domaine de la sécurité, l'open source fait partie intégrante de la blue team comme de la red team. Dans cette présentation, je vais vous présenter les grandes initiatives open source orientées sécurité pour vous aider à sécuriser vos développements. De l'Open source security foundation à L'OWASP en passant par MITRE et pleins d'autres. 
```

### 13h30 - 14h25 (Amphi B) Comment automatiser ses tests d'accessibilité ?

**Abstract**

Speaker : Anthony Le Goas

```
Les tests d'accessibilités web : on en voit trop peu dans nos chaînes de CI/CD et pourtant, il est possible de faire pas mal de choses ! Les outils et techniques pour automatiser des tests d'accessibilité sont multiples mais relativement méconnus des développeurs web. Que pouvons-nous tester, et comment ? Etudions tout cela ensemble, regardons comment intégrer ces tests dans nos pipelines (Gitlab CI, GitHub Actions, ...).
```

### 14h35 - 15h30 (Amphi D) Always-Green Refactoring : Retravailler sa codebase en douceur et sans stress

**Abstract**

Speaker : Thomas Carpaye, Mohammed Lamzira

```
Est-ce que toi aussi dans ta codebase tu as des morceaux de code qui te donnent des sueurs froides ? Des territoires anxiogènes que tu fuis comme la peste et que personne n'ose toucher ? D'habitude, le refactoring c’est des semaines de frustration et de souffrance, avec la CI en PLS et des bugs en abondance.

De toutes façons, personne ne t'accorde du temps pour retravailler ce code qui a l'air de fonctionner en production.

Viens coder avec nous pour apprendre des techniques simples comme la méthode mikado, les baby step, etc. tout en restant toujours au vert. Découvre nos outils pour te ré approprier l'enfer de ton legacy en un minimum de temps et en faire un terrain de jeu où tu pourras refactorer avec confiance.
```

### 16h - 16h25 (Amphi D) Une autre manière de tester vos microservices

**Abstract**

Speaker : Gwendal Leclerc

```
Dans une architecture microservices, l’interdépendance entre les microservices et le couplage avec des services externes (base de données, cache, message queue, ...) peuvent rendre les tests très compliqués. Les tests unitaires vont souvent nécessiter de mocker beaucoup de choses de manière invasive, pour au final tester peu de code métier, sans pouvoir tout couvrir.

Pour répondre à cette problématique, dans l’équipe domaine d’OVHcloud, nous avons mis en place une manière de tester nos différents services en “boîte grise”. Dans ce talk nous explorerons les concepts derrière cette stratégie de test, ainsi que ses avantages et inconvénients.

Nous verrons comment la mettre en œuvre via quelques outils :

    - Un peu de Shell et de Docker
    - Venom (https://github.com/ovh/venom) : un test runner déclaratif qui permet de faire des appels HTTP, manipuler des bases de données, et plein d’autres choses
    - Smocker (https://smocker.dev/guide/) : un server de mocks HTTP déclaratif pilotable via API

Vous découvrirez ainsi comment tester un microservice de manière complètement isolée, puis comment tester un ensemble de microservices interconnectés.
```

### 16h30 - 17h (Amphi C) Keynote

**Abstract**

Speaker :

```

```