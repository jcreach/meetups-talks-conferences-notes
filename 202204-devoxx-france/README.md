# Devoxx France 2022

## Table of content

[[_TOC_]]

##  Vendredi

### 10h45 - 11h30 (Maillot) Comment OpenTelemetry peut transformer votre monitoring en unifiant vos logs/metrics/traces 

**Abstract**

Speaker : [Vincent Behar](https://cfp.devoxx.fr/2022/speaker/vincent_behar)

Développeur passionné, j’ai toujours vécu un peu à cheval entre le monde des devs et celui des ops. Mes sujets de prédilection sont l’intégration et le déploiement continu, ainsi que le monitoring. Je travaille actuellement chez Ubisoft, où je participe à la construction d’une plateforme d’hébergement interne, basée sur Kubernetes. Je suis également un fervent partisan de l’open-source : je contribue régulièrement lorsque j’en ait l’occasion et je maintiens quelques projets. J’ai notamment contribué au projet Jenkins X, en développant le dashboard et toute la partie “observability”. J’ai également développé et je maintiens “Octopilot”, un outil d’automatisation de Pull Requests pour GitHub dans un contexte GitOps.

```
Si vous passez votre temps à jongler entre différents systèmes pour voir vos logs, metrics et peut-être traces, alors il est temps de faire quelque chose et cette session est pour vous !

Basé sur notre expérience à Ubisoft, nous allons voir pourquoi il est intéressant de revoir votre approche du monitoring, et comment une instrumentation de votre code basée sur OpenTelemetry, couplée avec l’utilisation de l’OpenTelemetry Collector et d’une plateforme de visualisation unifiée type Grafana peut transformer votre vision du monitoring.

OpenTelemetry est un projet open-source en pleine croissance: c’est le 2ème projet le plus actif de la Cloud Native Computing Foundation, après Kubernetes. Et il est en train de s’imposer comme véritable standard à plusieurs niveaux: format d’échange des données, convention de nommage, collecte et transformations, etc.

Dans cette présentation, nous aborderons le point de vue des devs qui produisent des logs/métrics/traces, mais également celui des opérateurs qui profiteront d’un agent unique qui est en fait une véritable plateforme d’enrichissement et de routage des données. Et surtout nous verrons comment impliquer tout le monde dans ce changement.
```

**Notes**

TO BE COMPLETED

### 11h45 - 12h30 (Neuilly 252 AB) À la découverte des Docker Dev Environments 

**Abstract**

Speakers : [Guillaume Lours](https://cfp.devoxx.fr/2022/speaker/guillaume_lours), [Djordje Lukic](https://cfp.devoxx.fr/2022/speaker/djordje_lukic)

**Guillaume :** Sr Software Engineer at Docker, after playing with Docker Desktop, Docker Hub and different CLI tools, I'm now working on features to improve developer teams collaboration such as Dev Environments

**Djordje :** Staff Software Engineer at Docker, loves making developer tools

```
Imaginez-vous en plein travail sur une nouvelle fonctionnalité et vous devez absolument faire une revue de code d'un de vos collègues. Vous allez encore une fois mettre de côté votre code en cours, récupérer celui de votre collègue et qui sait peut-être modifier votre environnement local pour tester ses changements ?

Et si nous vous proposions une nouvelle expérience de développement ? Comment ? Et pourquoi pas par un simple Copier/Coller de l'url de votre repository GIT dans Docker Desktop ?

Les Dev Environments sont une manière d'isoler votre code, vos dépendances et processus en cours, vous permettant ainsi d'avoir plusieurs versions d'un même projet en test sur votre machine. Et bien plus encore, partagez simplement votre code avec les autres membres de votre équipe, interagissez via Docker Compose avec une stack applicative complexe ...

```

**Notes**

TO BE COMPLETED

### 13h30 - 14h15 (Amphi bleu) Mieux maitriser TLS, OpenSSL et les certificats

**Abstract**

Speaker : [Mathieu Humbert](https://cfp.devoxx.fr/2022/speaker/mathieu_humbert)

Tech lead @Accenture & développeur passionné depuis plus de 15 ans. J'ai la chance de pouvoir travailler sur un projet alliant innovation et challenges techniques avec une architecture distribuée, des bases de donnée NoSQL, un vrai mode agile et surtout une équipe fun !

```
Aussi indispensable que soit le TLS aujourd’hui, on ne va pas se mentir, le sujet fait peur quand on ne le maitrise pas. Que ce soient les problèmes de certificats, un serveur à configurer proprement ou, de plus en plus, le 2-ways TLS, ces sujets nécessites de bonnes bases sur les mécanismes sous-jacent. Je vous propose donc de revenir sur comment fonctionne le protocole TLS, son évolution avec les dernières versions ainsi que la gestion des chaines de certificats. Une fois les bases posées, on abordera les problèmes les plus fréquemment rencontrés ainsi que l’outillage pour les débuguer. En somme, un tour d’horizon pour être mieux équipé et comprendre ce sujet complexe.
```

**Notes**

TO BE COMPLETED

### 14h30 - 15h15 (Neuilly 251) Enrichir son application web sans toucher au code source, c'est possible grâce au nouveau standard Web Extension API ! 

**Abstract**

Speakers : [Adrien Lasselle](https://cfp.devoxx.fr/2022/speaker/adrien_lasselle), [Anthony Pena](https://cfp.devoxx.fr/2022/speaker/anthony_pena)

**Adrien :** Je suis un développeur fullstack depuis 13 ans et je viens de Nantes. Je suis également membre organisateur et trésorier de l'association Blockchain & Société : https://blockchainsociete.org/ Nous organisons des meetups autours de la blockchain et des cryptos. De nature très curieux, j'adore échangé avec mes pairs et toujours en apprendre plus. Je trouve que je suis dans un secteur passionnant qui tend souvent au changement, c'est très enrichissant intellectuellement.

**Anthony :** Codeur et blogueur le jour et dévoreur de manga la nuit, vous me verrez souvent parler de Java, JavaScript, Typescript, Rust ou de test, parfois un peu (beaucoup) de jeux-vidéos ou de bricolage de console.

```
Aujourd'hui, les extensions web sont connues mais finalement peu utilisées car jugées trop liées au navigateur. A travers ce talk, nous vous proposons de vous présenter ce nouveau standard adopté par tous les navigateurs modernes. On va ENFIN pouvoir mutualiser nos outils et enrichir nos applications. Ce talk vous présentera donc un standard du web ainsi que des uses-cases concrets pouvant être utilisés dans un contexte professionnel.
```

**Notes**

TO BE COMPLETED

### 16h45 - 17h30 (Neuilly 252 AB) Pourquoi DevOps ne tient pas ses promesses ?

**Abstract**

Speakers : [Gérôme Egron](https://cfp.devoxx.fr/2022/speaker/gerome_egron), [Guillaume Mathieu](https://cfp.devoxx.fr/2022/speaker/guillaume_mathieu)

**Gérôme :** Gérôme est Cloud Advisor chez WeScale. Il accompagne ses clients dans la résolution de problèmes techniques, organisationnels ou humains et parfois tous en même temps.

**Guillame :** J'ai commencé dans à travailler dans l'IT en 2008 en faisant de l'automatisation de tests logiciels. Intéressé par les méthodes, je suis devenu Scrum Master et j'accompagnais les équipes Produit. Après plusieurs années de coaching (SM, PO et équipes), je suis toujours passionné par le travail collaboratif et ses apports au quotidien.

```
La plupart des clients que nous accompagnons disent faire du DevOps. Pourtant la réalité est loin de nous apporter satisfaction et les bénéfices attendus d'un point de vue théorique ne sont pas au rendez-vous.

Quelles sont les promesses de DevOps et pourquoi ne sont-elles pas tenues ? Est-ce que le monde des Devs est incompatible avec le monde des Ops ou est-ce que le problème est ailleurs ?

Nous allons partir des bases pour comprendre ce qu’il y a derrière le mot DevOps et partager nos expériences afin d'explorer les pratiques, bonnes ou mauvaises, rencontrées sur le terrain.

Après notre slot, vous aurez une compréhension claire du DevOps ainsi que des trucs et astuces pour amorcer cette transformation culturelle en évitant les pièges les plus communs.
```

**Notes**

TO BE COMPLETED
